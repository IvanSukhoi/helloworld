using System;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace HelloWorld.Tests
{
    public class ConsoleWriterTest
    {
        private StringWriter _stringWriter;

        [SetUp]
        public void Setup()
        {
            _stringWriter = new StringWriter();
            Console.SetOut(_stringWriter);
        }

        [TestCase("Hello World", 2)]
        public void Write_WriteStringsFromNewLine_Correct(string outputString, int quantity)
        {
            new ConsoleWriter().Write(outputString, quantity);

            var expectedString = string.Concat(Enumerable.Repeat(string.Format(outputString + "{0}", Environment.NewLine), quantity));

            Assert.That(_stringWriter.ToString(), Is.EqualTo(expectedString));
        }
    }
}