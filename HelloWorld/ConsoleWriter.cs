﻿using System;

namespace HelloWorld
{
    public class ConsoleWriter
    {
        public void Write(string str, int quantity = 1)
        {
            for(var i = 0; i < quantity; i++)
            {
                Console.WriteLine(str);
            }
        }
    }
}
